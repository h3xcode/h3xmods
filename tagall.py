# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2
import asyncio
from telethon import events
from telethon.tl.types import ChannelParticipantsAdmins, ChannelParticipantsBots
from uniborg.util import admin_cmd


@borg.on(admin_cmd("tagall"))
async def _(event):
    if event.fwd_from:
        return
    mentions = ""
    counter = 0
    chat = await event.get_input_chat()
    async for x in borg.iter_participants(chat, 100):
        mentions += f" \n [{x.first_name}](tg://user?id={x.id})"
        counter += 1
        if counter == 4:
            await event.reply(mentions)
            counter = 0
            mentions = ""
    if counter == 0:
        await event.delete()
        return
    await event.reply(mentions)
    await event.delete()


@borg.on(admin_cmd("admin"))
async def _(event):
    if event.fwd_from:
        return
    mentions = ""
    counter = 0
    chat = await event.get_input_chat()
    async for x in borg.iter_participants(chat, filter=ChannelParticipantsAdmins):
        mentions += f" \n [{x.first_name}](tg://user?id={x.id})"
        counter += 1
        if counter == 4:
            reply_message = None
            if event.reply_to_msg_id:
                reply_message = await event.get_reply_message()
                await reply_message.reply(mentions)
            else:
                await event.reply(mentions)
            counter = 0
            mentions = ""
    reply_message = None
    if counter == 0:
        await event.delete()
        return
    if event.reply_to_msg_id:
        reply_message = await event.get_reply_message()
        await reply_message.reply(mentions)
    else:
        await event.reply(mentions)
    await event.delete()

@borg.on(admin_cmd("bot"))
async def _(event):
    if event.fwd_from:
        return
    mentions = ""
    counter = 0
    chat = await event.get_input_chat()
    async for x in borg.iter_participants(chat, filter=ChannelParticipantsBots):
        mentions += f" \n [{x.first_name}](tg://user?id={x.id})"
        counter += 1
        if counter == 4:
            reply_message = None
            if event.reply_to_msg_id:
                reply_message = await event.get_reply_message()
                await reply_message.reply(mentions)
            else:
                await event.reply(mentions)
            counter = 0
            mentions = ""
    reply_message = None
    if counter == 0:
        await event.delete()
        return
    if event.reply_to_msg_id:
        reply_message = await event.get_reply_message()
        await reply_message.reply(mentions)
    else:
        await event.reply(mentions)
    await event.delete()

